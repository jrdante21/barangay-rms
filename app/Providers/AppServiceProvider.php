<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::enforceMorphMap([
            'business' => 'App\Models\ResidentBusiness',
            'asset' => 'App\Models\ResidentAsset',
        ]);

        // $this->app->bind('path.public', function() {
        //     return base_path().'/..';
        // });
    }
}
