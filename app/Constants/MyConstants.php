<?php

namespace App\Constants;

class MyConstants
{
    const DOCUMENT_TYPES = [
        1 => "Community Tax",
        2 => "Barangay Certificate",
        3 => "Barangay Clearance",
        4 => "Barangay Indigency",
        5 => "Business Permit",
        6 => "Business Clearance",
        // 7 => "Purok Clearance",
        8 => "Certification",
        9 => "COVID Travel Pass",
        10 => "Summons",
        11 => "Borrowed Materials",
    ];

    const OFFCIAL_POSITIONS = [
        1 => "Barangay Captain",
        2 => "Barangay Secretary",
        3 => "Barangay Treasurer",
        4 => "Chairman for Women & Health", 
        5 => "Chairman for Laws & Ordinance",
        6 => "Chairman for Budget & Appropriation", 
        7 => "Chairman for Education",
        8 => "Chairman for Infrastructure",
        9 => "Chairman for Agriculture",
        10 => "Chairman for Peace & Order",
        11 => "SK Chairman",
        12 => "Purok President",
        13 => "Clerk 1",
        14 => "Clerk 2",
    ];
}