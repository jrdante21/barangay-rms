<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class Resident extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    protected $hidden = ['password'];
    protected $fillable = [
        'fname', 'mname', 'lname',
        'gender', 'civil_status', 'bday',
        'home_number', 'purok', 'barangay', 'municipality', 'province', 'years_resided',
        'is_voter', 'is_household',
        'father_name', 'mother_name', 'spouse_name',
        'email', 'password', 'email_verified_at'
    ];
    protected $appends = ['fullname','address','gender_str','civil_status_str','age','father','mother','spouse'];
    protected $casts = [
        'is_voter' => 'boolean',
        'is_household' => 'boolean',
        'father_name' => 'array',
        'mother_name' => 'array',
        'spouse_name' => 'array',
    ];

    public function getFullnameAttribute()
    {
        return Str::title(trim("{$this->fname} {$this->mname} {$this->lname}"));
    }

    public function getAddressAttribute()
    {
        return Str::title("{$this->home_number}, Purok {$this->purok}, {$this->barangay}, {$this->municipality}, {$this->province}");
    }

    public function getGenderStrAttribute()
    {
        return ($this->gender <= 1) ? 'Male' : 'Female';
    }

    public function getCivilStatusStrAttribute()
    {
        switch ($this->civil_status) {
            case 1: return 'Single'; break;
            case 2: return 'Married'; break;
            case 3: return 'Lived In'; break;
            default: return 'Widowed'; break;
        }
    }

    public function getAgeAttribute () {
        $diff = date_diff(date_create("now"), date_create($this->bday));
        return $diff->y;
    }

    public function getFatherAttribute()
    {
        if (empty($this->father_name)) return 'N / A';
        $name = $this->father_name;
        return Str::title(trim("{$name['fname']} {$name['mname']} {$name['lname']}"));
    }

    public function getMotherAttribute()
    {
        if (empty($this->mother_name)) return 'N / A';
        $name = $this->mother_name;
        return Str::title(trim("{$name['fname']} {$name['mname']} {$name['lname']}"));
    }

    public function getSpouseAttribute()
    {
        if (empty($this->spouse_name)) return 'N / A';
        $name = $this->spouse_name;
        return Str::title(trim("{$name['fname']} {$name['mname']} {$name['lname']}"));
    }

    public function educations()
    {
        return $this->hasMany(ResidentEducation::class)->orderByRaw("CONCAT(id, '-', level)");
    }

    public function children()
    {
        return $this->hasMany(ResidentChildren::class);
    }

    public function businesses()
    {
        return $this->hasMany(ResidentBusiness::class);
    }

    public function jobs()
    {
        return $this->hasMany(ResidentJob::class);
    }

    public function assets()
    {
        return $this->hasMany(ResidentAsset::class);
    }

    public function siblings()
    {
        return $this->hasMany(ResidentSibling::class);
    }

    public function emailcodes()
    {
        return $this->hasMany(ResidentEmailcode::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function scopeSearchByName($query, $search='')
    {
        if (empty($search)) return $query;
        $search = '%'.$search.'%';
        return $query
        ->whereRaw("
            CONCAT(fname, ' ', mname, ' ', lname) LIKE ? OR
            CONCAT(fname, ' ', lname) LIKE ? OR
            CONCAT(lname, ' ', fname) LIKE ?
        ", [$search, $search, $search]);
    }

    public function scopeWhereStatus($query, $status=0)
    {
        if (empty($status)) return $query;
        return ($status >= 2) ? 
            $query->whereNull('email_verified_at'): 
            $query->whereNotNull('email_verified_at');
    }
}
