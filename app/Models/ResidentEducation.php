<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ResidentEducation extends Model
{
    use HasFactory;
    public $table = 'resident_educations';
    public $timestamps = false;

    protected $fillable = ['level','course','school_name','school_address','graduated_at'];
    protected $appends = ['level_str','address'];
    protected $casts = ['school_address' => 'array'];

    public function getLevelStrAttribute()
    {
        switch ($this->level) {
            case 1: return 'Primary'; break;
            case 2: return 'Secondary'; break;
            case 3: return 'Tertiary'; break;
            case 4: return 'ALS'; break;
            default: return 'Vocational'; break;
        }
    }

    public function getAddressAttribute()
    {
        $address = $this->school_address;
        return Str::title("{$address['barangay']}, {$address['municipality']}, {$address['province']}");
    }
}
