<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TransactionSummon extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['resident_id', 'resident_name', 'scheduled_at'];
    protected $appends = [
        'no_name',
        'scheduled_time',
        'scheduled_date',
        'respondent'
    ];

    protected static function booted()
    {
        static::addGlobalScope('respondent', function (Builder $builder) {
            $respondent = Resident::selectRaw("
                id as respondent_id,
                CONCAT(fname, ' ', mname, ' ', lname) as respondent_name
            ");

            $builder->leftJoinSub($respondent, 'respondent', function($join){
                $join->on('transaction_summons.resident_id', '=', 'respondent.respondent_id');
            });
        });
    }

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }

    public function getNoNameAttribute()
    {
        return (empty($this->resident_id)) ? true: false;
    }

    public function getScheduledTimeAttribute()
    {
        return now()->parse($this->scheduled_at)->getTimestampMs();
    }

    public function getScheduledDateAttribute()
    {
        return now()->parse($this->scheduled_at)->format('Y-m-d');
    }

    public function getRespondentAttribute()
    {
        return (empty($this->resident_id)) ? $this->resident_name : $this->respondent_name;
    }
}
