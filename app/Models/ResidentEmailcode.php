<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResidentEmailcode extends Model
{
    use HasFactory;
    protected $fillable = ['code','used_at'];

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }

    public function scopeWhereCodeUnused($query, $code)
    {
        return $query->where('code', $code)->whereNull('used_at');
    }
}
