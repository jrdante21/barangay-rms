<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ResidentChildren extends Model
{
    use HasFactory;
    public $table = 'resident_children';
    public $timestamps = false;

    protected $fillable = [
        'fname', 'mname', 'lname',
        'gender', 'civil_status', 'bday',
        'is_student', 'school_level', 'school_name', 'school_address'
    ];
    protected $appends = ['fullname','school_address_str','gender_str','civil_status_str','age','level_str'];
    protected $casts = ['school_address' => 'array', 'is_student' => 'boolean'];

    public function getFullnameAttribute()
    {
        return Str::title(trim("{$this->fname} {$this->mname} {$this->lname}"));
    }

    public function getSchoolAddressStrAttribute()
    {
        if (!$this->is_student) return '';

        $address = $this->school_address;
        return Str::title("{$address['barangay']}, {$address['municipality']}, {$address['province']}");
    }

    public function getGenderStrAttribute()
    {
        return ($this->gender <= 1) ? 'Male' : 'Female';
    }

    public function getCivilStatusStrAttribute()
    {
        switch ($this->civil_status) {
            case 1: return 'Single'; break;
            case 2: return 'Married'; break;
            case 3: return 'Lived In'; break;
            default: return 'Widowed'; break;
        }
    }

    public function getAgeAttribute () {
        $diff = date_diff(date_create("now"), date_create($this->bday));
        return $diff->y;
    }

    public function getLevelStrAttribute()
    {
        switch ($this->school_level) {
            case 1: return 'Primary'; break;
            case 2: return 'Secondary'; break;
            case 3: return 'Tertiary'; break;
            default: return 'Vocational'; break;
        }
    }
}
