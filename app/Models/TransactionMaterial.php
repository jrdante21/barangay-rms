<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionMaterial extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['material', 'status', 'quantity'];
    protected $casts = [
        'amount' => 'float'
    ];
    protected $appends = [
        'status_details'
    ];

    public function getStatusDetailsAttribute()
    {
        switch ($this->status) {
            case 1: 
                return ['text' => 'Good', 'type' => 'success']; 
                break;
            case 2: 
                return ['text' => 'Damaged', 'type' => 'warning']; 
                break;
            default: 
                return ['text' => 'Lost', 'type' => 'error']; 
                break;
        }
    }
}
