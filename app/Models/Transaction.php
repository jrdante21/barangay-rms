<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use App\Constants\MyConstants;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'resident_id', 'type', 'ctc_number', 'or_number', 'amount', 'purpose',
        'issued_at', 'paid_at', 'returned_at', 'requested_at', 'is_requested'
    ];
    protected $casts = [
        'amount' => 'float',
        'is_requested' => 'boolean'
    ];
    protected $appends = [
        'title',
        'resident_business_id',
        'resident_asset_id',
        'remarks',
    ];

    protected static function booted()
    {
        static::addGlobalScope('material_status', function (Builder $builder) {
            $status = TransactionMaterial::selectRaw("
                transaction_id as transaction_material_status_id,
                status as transaction_material_status
            ");

            $builder->leftJoinSub($status, 'material_status', function($join){
                $join->on('transactions.id', '=', 'material_status.transaction_material_status_id');
            });
        });
    }

    public function getTitleAttribute()
    {
        $types = MyConstants::DOCUMENT_TYPES;
        return (empty($types[$this->type])) ? 'None' : $types[$this->type];
    }

    public function getResidentBusinessIdAttribute()
    {
        return (in_array($this->type, [5,6])) ? $this->resident_itemable_id : null;
    }

    public function getResidentAssetIdAttribute()
    {
        return ($this->type == 8) ? $this->resident_itemable_id : null;
    }

    public function getRemarksAttribute()
    {
        if (!$this->is_requested && $this->type != 11) return [
            'text' => "Compeleted",
            'type' => 'success',
            'issued' => true,
            'paid' => true
        ];

        switch ($this->type) {
            case 1: // Community Tax
                if (!empty($this->issued_at) && !empty($this->paid_at)) {
                    return ['text' => "Request Completed", 'type' => 'success', 'paid' => true, 'issued' => true];
                } elseif (!empty($this->issued_at) && empty($this->paid_at)) {
                    return ['text' => "Unpaid", 'type' => 'error', 'issued' => true];
                } else {
                    return ['text' => "Pending", 'type' => 'warning'];
                }
                break;

            case 11: // Borrowed Materials
                return (!empty($this->returned_at)) ?
                    ['text' => "Returned", 'type' => 'success', 'paid' => true, 'issued' => true]:
                    ['text' => "Not yet returned", 'type' => 'error', 'issued' => true];
                break;
            
            default: // Other docs
                return (!empty($this->issued_at)) ?
                    ['text' => "Request Completed", 'type' => 'success', 'paid' => true, 'issued' => true]:
                    ['text' => "Pending", 'type' => 'warning', 'paid' => true,];
                break;
        }
    }

    public function resident()
    {
        return $this->belongsTo(Resident::class);
    }

    public function resident_itemable()
    {
        return $this->morphTo();
    }

    public function covid()
    {
        return $this->hasOne(TransactionCovid::class);
    }

    public function summon()
    {
        return $this->hasOne(TransactionSummon::class);
    }
    
    public function material()
    {
        return $this->hasOne(TransactionMaterial::class);
    }

    public function scopeRelationsByType($query, $type)
    {
        return $query
        ->when(in_array($type, [5,6,8]), fn($q) => $q->with('resident_itemable')) // Business and Asset of resident
        ->when($type == 9, fn($q) => $q->with('covid')) // Covid Travel Pass
        ->when($type == 10, fn($q) => $q->with('summon')) // Summon
        ->when($type == 11, fn($q) => $q->with('material')); // Materials
    }

    public function scopeRequested($query)
    {
        return $query->where('is_requested', 1);
    }

    public function scopeIncompleteRequest($query)
    {
        return $query->where(function($query)
        {
            // Community tax
            $query->where(function($query){
                $query->where('type', 1)
                ->where('is_requested', 1)
                ->whereRaw("(
                    issued_at IS NULL OR paid_at IS NULL
                )");
            })

            // Borrowed materials
            ->orWhere(function($query){
                $query->where('type', 11)
                ->whereNull('returned_at');
            })

            // Others
            ->orWhere(function($query){
                $query->whereNotIn('type', [1,11])
                ->where('is_requested', 1)
                ->whereNull('issued_at');
            });
        });
    }
}
