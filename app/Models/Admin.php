<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;
    
    protected $hidden = ['password'];
    protected $fillable = ['username','password','is_super'];
    protected $appends = ['role'];
    protected $casts = [
        'is_super' => 'boolean'
    ];

    public function getRoleAttribute()
    {
        return ($this->is_super) ? 'superadmin' : 'admin';
    }
}
