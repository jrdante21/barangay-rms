<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Setting extends Model
{
    use HasFactory;
    protected $appends = ['address'];

    public function getAddressAttribute()
    {
        return Str::title(trim("{$this->barangay}, {$this->municipality}, {$this->province}"));
    }

}
