<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TransactionCovid extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['province', 'municipality', 'barangay'];
    protected $appends = [
        'address'
    ];
    
    public function getAddressAttribute()
    {
        return Str::title("{$this->barangay}, {$this->municipality}, {$this->province}");
    }

}
