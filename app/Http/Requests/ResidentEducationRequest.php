<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResidentEducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'level' => ['required', 'numeric', 'min:1'],
            'course' => ['required', 'min:3'],
            'school_name' => ['required', 'min:5'],
            'school_address.province' => ['required', 'min:3'],
            'school_address.municipality' => ['required', 'min:3'],
            'school_address.barangay' => ['required', 'min:3'],
            'graduated_at' => ['required', 'date'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'graduated_at' => date('Y-m-d', strtotime($this->graduated_at)),
            'school_address.barangay' => strtoupper($this->school_address['barangay']),
            'school_address.municipality' => strtoupper($this->school_address['municipality']),
            'school_address.province' => strtoupper($this->school_address['province']),
        ]);
    }
}
