<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RequestDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $residentId = auth()->guard('resident')->id();

        return [
            'type' => ['required', 'numeric', 'min:1', 'max:11'],
            
            // Business
            'is_business' => ['required', 'boolean'],
            'resident_business_id' => ['exclude_if:is_business,false', 'required',
                Rule::exists('resident_businesses', 'id')->where(fn($query) => $query->where('resident_id', $residentId))
            ],

            // Asset
            'is_asset' => ['required', 'boolean'],
            'resident_asset_id' => ['exclude_if:is_asset,false', 'required',
                Rule::exists('resident_assets', 'id')->where(fn($query) => $query->where('resident_id', $residentId))
            ],

            // Covid
            'is_covid' => ['required', 'boolean'],
            'covid.barangay' => ['exclude_if:is_covid,false', 'required', 'min:3'],
            'covid.municipality' => ['exclude_if:is_covid,false', 'required', 'min:3'],
            'covid.province' => ['exclude_if:is_covid,false', 'required', 'min:3'],

            // Summon
            'is_summon' => ['required', 'boolean'],
            'is_summon_hasname' => ['required', 'boolean'],
            'is_summon_noname' => ['required', 'boolean'],
            'summon.no_name' => ['exclude_if:is_summon,false', 'required', 'boolean'],
            'summon.resident_id' => ['exclude_if:is_summon_hasname,false', 'required', 'exists:residents,id', 'different:resident_id'],
            'summon.resident_name' => ['exclude_if:is_summon_noname,false', 'required', 'min:5'],
            'summon.scheduled_at' => ['exclude_if:is_summon,false', 'required', 'date'],

            // Has Purpose
            'has_purpose' => ['required', 'boolean'],
            'purpose' => ['exclude_if:has_purpose,false', 'required', 'min:5'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'is_business' => in_array($this->type, [
                5,6 // Business Certificate and Clearance
            ]),
            'is_asset' => $this->type == 8, // Asset Certification
            'is_covid' => $this->type == 9, // Covid Travel Pass

            // Summon
            'is_summon' => $this->type == 10, 
            'is_summon_hasname' => $this->type == 10 && !$this['summon.no_name'],
            'is_summon_noname' => $this->type == 10 && $this['summon.no_name'],

            'has_purpose' => in_array($this->type, [
                8, // Asset Certification
                9, // Covid Travel Pass
                10, // Summons
            ]),
        ]);
    }

    public function attributes()
    {
        return [
            'summon.resident_id' => 'respondent'
        ];
    }
}
