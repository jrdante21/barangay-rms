<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules\AlphaSpace;

class ResidentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $resident = $this->route('resident');
        $fields = [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'gender' => ['required', 'numeric', 'min:1'],
            'civil_status' => ['required', 'numeric', 'min:1'],
            'home_number' => ['required', 'min:1', 'max:20'],
            'purok' => ['required', 'numeric', 'min:1', 'max:10'],
            'bday' => ['required', 'date'],
            'barangay' => ['required', 'min:3'],
            'municipality' => ['required', 'min:3'],
            'province' => ['required', 'min:3'],
            'years_resided' => ['required', 'numeric'],
            'is_voter' => ['required', 'boolean'],
            'is_household' => ['required', 'boolean'],
            'email' => ['required', 'email', Rule::unique('residents', 'email')->ignore($resident)],
        ];
        if (empty($resident)) $fields['password'] = ['required', 'min:5'];

        return $fields;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'bday' => date('Y-m-d', strtotime($this->bday)),
            'barangay' => strtoupper($this->barangay),
            'municipality' => strtoupper($this->municipality),
            'province' => strtoupper($this->province),
        ]);
    }
}
