<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResidentJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => ['required', 'min:5'],
            'company_name' => ['required', 'min:3'],
            'started_at' => ['required', 'date'],
            'is_present' => ['required', 'boolean'],
            'ended_at' => ['required_if:is_present,false', 'date'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'started_at' => date('Y-m-d', strtotime($this->started_at)),
            'ended_at' => date('Y-m-d', strtotime($this->ended_at)),
        ]);
    }
}
