<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResidentBusinessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:5'],
            'address.province' => ['required', 'min:3'],
            'address.municipality' => ['required', 'min:3'],
            'address.barangay' => ['required', 'min:3'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'address.barangay' => strtoupper($this->address['barangay']),
            'address.municipality' => strtoupper($this->address['municipality']),
            'address.province' => strtoupper($this->address['province']),
        ]);
    }

    public function withValidator($validator)
    {
        $business = $this->route('business');
        if (empty($business)) return;

        $transaction = $business->transactionDocument;
        $validator->after(function ($validator) use ($transaction) {
            if (!empty($transaction)) {
                $validator->errors()->add('has_transaction', 'Business already been used in transactions, cannot be modified.');
            }
        });
    }
}
