<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Constants\MyConstants;
use App\Models\Setting;

class ViewShare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        View::share('documentTypes', MyConstants::DOCUMENT_TYPES);
        View::share('officialPositions', MyConstants::OFFCIAL_POSITIONS);
        View::share('barangaySettings', Setting::first());

        return $next($request);
    }
}
