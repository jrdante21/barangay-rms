<?php

namespace App\Http\Controllers\Logins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:web,official')->except('logout');
    }

    public function index()
    {
        return view('admin');
    }

    public function login(Request $request)
    {
        $valid = $request->validate([
            'username' => 'required',
            'password' => 'required',
            'is_admin' => 'required|boolean'
        ]);

        $guard = ($valid['is_admin']) ? 'web' : 'official';
        unset($valid['is_admin']);
        if (auth()->guard($guard)->attempt($valid)) {
            return auth()->guard($guard)->user();

        } else {
            return response("Username and password is incorrect.", 422);
        }
    }

    public function logout()
    {
        auth()->logout();
        auth()->guard('official')->logout();
        return redirect()->route('admin.login');
    }
}
