<?php

namespace App\Http\Controllers\Logins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResidentController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:resident')->except('logout');
    }

    public function index()
    {
        return view('resident');
    }

    public function login(Request $request)
    {
        $valid = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (auth()->guard('resident')->attempt($valid)) {
            $user = auth()->guard('resident')->user();
            if (empty($user->email_verified_at)) {
                auth()->guard('resident')->logout();
                return response("Email not yet verified! Please check your email code verification.", 422);
            }

            return $user;

        } else {
            return response("Email or password is incorrect.", 422);
        }
    }

    public function logout()
    {
        auth()->guard('resident')->logout();
        return redirect()->route('resident.login');
    }
}
