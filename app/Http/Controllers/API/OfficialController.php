<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Official;
use App\Http\Requests\OfficialUpsertRequest;

class OfficialController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function index(Request $request)
    {
        $officials = Official::with('signatories')
            ->withTrashed()
            ->when($request->search, fn($query, $search) => $query->searchByName($search))
            ->when($request->all,
                fn($query) => $query->get(),
                fn($query) => $query->paginate(20)
            );
        return $officials;
    }

    public function store(OfficialUpsertRequest $request)
    {
        $valid = $request->validated();
        $valid['password'] = bcrypt($valid['password']);
        $official = Official::create($valid);
        return $official;
    }

    public function update(OfficialUpsertRequest $request, Official $official)
    {
        $valid = $request->validated();
        if (!empty($valid['password'])) $valid['password'] = bcrypt($valid['password']);
        $official->update($valid);
        return $official;
    }

    public function destroy(Official $official)
    {
        $official->delete();
        return $official;
    }

    public function restore($official)
    {
        $official = Official::onlyTrashed()->findOrFail($official);
        $official->restore();
        return $official;
    }
}
