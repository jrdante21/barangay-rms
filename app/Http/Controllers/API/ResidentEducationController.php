<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Resident;
use App\Models\ResidentEducation;
use App\Http\Requests\ResidentEducationRequest;

class ResidentEducationController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin');
    }

    public function store(ResidentEducationRequest $request, Resident $resident)
    {
        $valid = $request->validated();
        $resident->educations()->create($valid);
        return $resident->educations;
    }

    public function update(ResidentEducationRequest $request, ResidentEducation $education)
    {
        $valid = $request->validated();
        $education->update($valid);
        return $education;
    }

    public function destroy(ResidentEducation $education)
    {
        $education->delete();
    }
}
