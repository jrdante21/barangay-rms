<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Resident;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Total residents by purok
        // $queryByPurok = DB::table('residents')->selectRaw("
        //     'purok' as category,
        //     purok as categ_type,
        //     COUNT(*) as count
        // ")
        // ->groupBy('purok');

        // Total residents by gender
        // $queryByGender = DB::table('residents')->selectRaw("
        //     'gender' as category,
        //     (CASE gender WHEN 1 THEN 'male' ELSE 'female' END) as categ_type,
        //     COUNT(*) as count
        // ")
        // ->groupBy('gender');

        // Total residents by voters
        $queryByVoters = DB::table('residents')->selectRaw("
            'voters' as category,
            'total' as categ_type,
            COUNT(*) as count
        ")
        ->where('is_voter',1);

        // Total covid travel pass issued
        $queryByCovid = DB::table('transactions')->selectRaw("
            'covid' as category,
            DATE_FORMAT(issued_at, '%Y%m') as categ_type,
            COUNT(*) as count
        ")
        ->groupBy('categ_type')
        ->where('type', 9)
        ->whereNotNull('issued_at');

        // Total residents
        $query = DB::table('residents')->selectRaw("
            'main' as category,
            'total' as categ_type,
            COUNT(*) as count
        ")
        // ->union($queryByPurok)
        // ->union($queryByGender)
        ->union($queryByVoters)
        ->union($queryByCovid)
        ->get();

        $query = collect($query)->groupBy('category')->all();
        $query['main'] = (!empty($query['main'])) ? $query['main'][0] : [];
        $query['voters'] = (!empty($query['voters'])) ? $query['voters'][0] : [];

        $queryByPurokGender = DB::table('residents')->selectRaw("
            purok,
            SUM(CASE gender WHEN 1 THEN 1 ELSE 0 END) as male,
            SUM(CASE gender WHEN 2 THEN 1 ELSE 0 END) as female
        ")
        ->groupBy('purok')
        ->get();
        $query['purok'] = $queryByPurokGender;

        return $query;
    }
}
