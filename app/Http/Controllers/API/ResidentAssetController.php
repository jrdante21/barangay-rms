<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Resident;
use App\Models\ResidentAsset;
use App\Http\Requests\ResidentAssetRequest;

class ResidentAssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function index(Resident $resident)
    {
        return $resident->assets;
    }

    public function store(ResidentAssetRequest $request, Resident $resident)
    {
        $valid = collect($request->validated());
        $resident->assets()->create([
            'type' => $valid->pull('type'),
            'descriptions' => $valid->all()
        ]);
        return $resident->assets;
    }

    public function update(ResidentAssetRequest $request, ResidentAsset $asset)
    {
        $valid = collect($request->validated());
        $asset->update([
            'type' => $valid->pull('type'),
            'descriptions' => $valid->all()
        ]);
        return $asset;
    }

    public function destroy(ResidentAsset $asset)
    {
        if(!empty($asset->transactionDocument)) return response(['message' => "Asset already been used in transactions, cannot be deleted."], 422);
        $asset->delete();
    }
}
