<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Resident;
use App\Models\ResidentBusiness;
use App\Http\Requests\ResidentBusinessRequest;

class ResidentBusinessController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function index(Resident $resident)
    {
        return $resident->businesses;
    }

    public function store(ResidentBusinessRequest $request, Resident $resident)
    {
        $valid = $request->validated();
        $resident->businesses()->create($valid);
        return $resident->businesses;
    }

    public function update(ResidentBusinessRequest $request, ResidentBusiness $business)
    {
        $valid = $request->validated();
        $business->update($valid);
        return $business;
    }

    public function destroy(ResidentBusiness $business)
    {
        if(!empty($business->transactionDocument)) return response(['message' => "Business already been used in transactions, cannot be deleted."], 422);
        $business->delete();
    }
}
