<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TransactionRequest;
use App\Models\Transaction;
use App\Models\ResidentAsset;
use App\Models\ResidentBusiness;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $type = $request->input('type', 1);
        $transactions = Transaction::with('resident')
        ->relationsByType($type)
        ->where('type', $type)
        ->latest()
        ->paginate(20);

        return $transactions;
    }

    public function store(TransactionRequest $request)
    {
        $valid = $request->validated();
        $valid['is_requested'] = 0;

        // Resident Itemables
        if ($valid['is_business']) {
            $business = ResidentBusiness::find($valid['resident_business_id']);
            $transaction = $business->transactions()->create($valid);
        
        } elseif ($valid['is_asset']) {
            $asset = ResidentAsset::find($valid['resident_asset_id']);
            $transaction = $asset->transactions()->create($valid);

        } else {
            $transaction = Transaction::create($valid);
        }

        if ($valid['is_covid']) $transaction->covid()->create($valid['covid']); // Covid Travel Pass
        if ($valid['is_summon']) { // Summon
            $summon = $valid['summon'];
            unset($summon['no_name']);
            $transaction->summon()->create($summon);
        }
        if ($valid['is_materials']) $transaction->material()->create($valid['material']); // Borrowed Materials

        $transaction = Transaction::relationsByType($valid['type'])->find($transaction->id);
        return $transaction;
    }

    public function update(TransactionRequest $request, $transaction)
    {
        $valid = $request->validated();
        $transaction = Transaction::relationsByType($valid['type'])->find($transaction);

        // Resident Itemables
        if ($valid['is_business']) {
            $valid['resident_itemable_id'] = $valid['resident_business_id'];

        } elseif ($valid['is_asset']) {
            $valid['resident_itemable_id'] = $valid['resident_asset_id'];
        }

        $transaction->update($valid);

        if ($valid['is_covid']) $transaction->covid()->update($valid['covid']); // Covid Travel Pass
        if ($valid['is_summon']) { // Summon
            $summon = $valid['summon'];
            if ($valid['is_summon_noname']) $summon['resident_id'] = null;
            unset($summon['no_name']);
            $transaction->summon()->update($summon);
        }
        if ($valid['is_materials']) $transaction->material()->update($valid['material']); // Borrowed Materials

        $transaction->refresh();
        return $transaction;
    }

    public function updatePayment(Request $request, Transaction $transaction)
    {
        $validate['paid_at'] = ['required', 'date'];
        if ($transaction->type == 11) $validate['returned_at'] = ['required', 'date'];

        $valid = $request->validate($validate);
        $transaction->update($valid);

        return $transaction;
    }
}
