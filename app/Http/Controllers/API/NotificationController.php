<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;

class NotificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $transactions = Transaction::selectRaw("
            type,
            SUM(CASE WHEN issued_at IS NULL THEN 1 ELSE 0 END) as pending,
            SUM(CASE WHEN issued_at IS NOT NULL THEN 1 ELSE 0 END) as unpaid
        ")
        ->groupBy('type')
        ->incompleteRequest()
        ->get();

        $transactions = collect($transactions)->map(function($transaction){
            return collect($transaction)
            ->only(['type','title','pending','unpaid'])
            ->all();
        })->all();

        return $transactions;
    }
}
