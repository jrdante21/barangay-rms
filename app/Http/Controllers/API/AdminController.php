<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin;
use App\Http\Requests\AdminRequest;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:superadmin');
    }

    public function index()
    {
        $admins = Admin::withTrashed()->where('is_super', 0)->get();
        return $admins;
    }

    public function store(AdminRequest $request)
    {
        $valid = $request->validated();
        $valid['is_super'] = 0;
        $valid['password'] = bcrypt($valid['password']);
        $admin = Admin::create($valid);
        return $admin;
    }

    public function update(AdminRequest $request, Admin $admin)
    {
        $valid = $request->validated();
        $valid['password'] = bcrypt($valid['password']);
        $admin->update($valid);
        return $admin;
    }

    public function destroy(Admin $admin)
    {
        $admin->delete();
        return $admin;
    }

    public function restore($admin)
    {
        $admin = Admin::onlyTrashed()->findOrFail($admin);
        $admin->restore();
        return $admin;
    }
}
