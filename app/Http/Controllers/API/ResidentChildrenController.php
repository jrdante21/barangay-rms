<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Resident;
use App\Models\ResidentChildren;
use App\Http\Requests\ResidentChildrenRequest;

class ResidentChildrenController extends Controller
{
    public function __construct()
    {
        $this->middleware('user:admin')->except(['index']);
    }

    public function store(ResidentChildrenRequest $request, Resident $resident)
    {
        $valid = $request->validated();
        $resident->children()->create($valid);
        return $resident->children;
    }

    public function update(ResidentChildrenRequest $request, ResidentChildren $child)
    {
        $valid = $request->validated();
        $child->update($valid);
        return $child;
    }

    public function destroy(ResidentChildren $child)
    {
        $child->delete();
    }
}
