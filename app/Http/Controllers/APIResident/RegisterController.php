<?php

namespace App\Http\Controllers\APIResident;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerificationCode;
use App\Models\Resident;
use App\Models\ResidentEmailcode;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $valid = $request->validated();
        $valid['password'] = bcrypt($valid['password']);
        $resident = Resident::create($valid);

        // Generate code
        $code = null;
        while (empty($code)) {
            $alphanum = collect(str_split('qwertyuiopasdfghjklzxcvbnm1234567890'))->shuffle()->all();
            $alphanum = collect($alphanum)->take(5)->all();
            $alphanum = collect($alphanum)->join('');
            $codes = ResidentEmailcode::where('code', $alphanum)->count();
            if ($codes <= 0) $code = $alphanum;
        }
        // Save Code
        $resident->emailcodes()->create(['code' => $code]);

        // Send to email
        Mail::to($resident->email)->send(new EmailVerificationCode($code));
    }
}
