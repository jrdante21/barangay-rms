<?php

namespace App\Http\Controllers\APIResident;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Resident;

class ResidentController extends Controller
{
    public function index()
    {
        $user = auth()->guard()->user();
        $user->educations;
        $user->children;
        $user->businesses;
        $user->jobs;
        $user->assets;
        $user->siblings;
        return view('resident', compact('user'));
    }

    public function updatePassword(Request $request)
    {
        $user = auth()->guard('resident')->user();
        $valid = $request->validate([
            'password_old' => [
                'required',
                function($attr, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->password)) $fail("The password is incorrect.");
                }],
            'password' => 'required|min:5|confirmed|different:password_old',
        ]);

        $user->update(['password' => bcrypt($valid['password'])]);
        return $user;
    }
}
