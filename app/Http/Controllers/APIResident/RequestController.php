<?php

namespace App\Http\Controllers\APIResident;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RequestDocumentRequest;
use App\Models\Transaction;
use App\Models\Resident;

class RequestController extends Controller
{
    public function index(Request $request)
    {
        $resident = auth()->guard('resident')->user();
        $requests = $resident
        ->transactions()
        ->requested()
        ->latest('updated_at')
        ->paginate(20);
        
        return $requests;
    }

    public function store(RequestDocumentRequest $request)
    {
        $valid = $request->validated();
        $valid['is_requested'] = 1;
        $valid['requested_at'] = now()->format('Y-m-d H:i:s');
        $valid['amount'] = 0;

        $resident = auth()->guard('resident')->user();
        $requestsCount = $resident->transactions()
        ->where('type', $valid['type'])
        ->incompleteRequest()
        ->count();

        // Prevent from multiple request
        if ($requestsCount >= 1) return response("You still have pending request for this certificate. Please wait for confirmation.", 422);

        // Resident Itemables
        if ($valid['is_business']) {
            $valid['resident_id'] = $resident->id;
            $business = $resident->businesses()->find($valid['resident_business_id']);
            $transaction = $business->transactions()->create($valid);
        
        } elseif ($valid['is_asset']) {
            $valid['resident_id'] = $resident->id;
            $asset = $resident->assets()->find($valid['resident_asset_id']);
            $transaction = $asset->transactions()->create($valid);

        } else {
            $transaction = $resident->transactions()->create($valid);
        }

        if ($valid['is_covid']) $transaction->covid()->create($valid['covid']); // Covid Travel Pass
        if ($valid['is_summon']) { // Summon
            $summon = $valid['summon'];
            unset($summon['no_name']);
            $transaction->summon()->create($summon);
        }

        $transaction = Transaction::relationsByType($valid['type'])->find($transaction->id);
        return $transaction;
    }
}
