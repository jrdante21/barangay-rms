<?php

namespace App\Http\Controllers\APIResident;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ResidentEmailcode;

class EmailCodeVerify extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $valid = $request->validate([
            'code' => [
                'required', 'min:5', 'max:5',
                function($attr, $value, $fail) {
                    $code = ResidentEmailcode::whereCodeUnused($value)->first();
                    if (empty($code)) $fail("Code not found.");
                }
            ]
        ]);

        $date = now();
        $code = ResidentEmailcode::whereCodeUnused($valid['code'])->first();
        $code->update(['used_at' => $date]);
        $code->resident()->update(['email_verified_at' => $date]);
        return $date;
    }
}
