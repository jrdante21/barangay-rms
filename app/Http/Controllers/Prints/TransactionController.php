<?php

namespace App\Http\Controllers\Prints;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Transaction;

class TransactionController extends Controller
{
    public function __invoke(Request $request)
    {
        $transactions = Transaction::with('resident')
            ->when($request->resident_id, fn($query, $resident_id) => $query->where('resident_id', $resident_id))
            ->when($request->type, function($query, $type){
                if ($type == 'all') return $query;
                return $query->where('type', $type);
            })
            ->whereRaw("DATE(issued_at) >= ? AND DATE(issued_at) <= ?",[
                date('Y-m-d', strtotime($request->date_start)),
                date('Y-m-d', strtotime($request->date_end))
            ])
            ->get();

        $total = collect($transactions)->reduce(function($carry, $item){
            $amount = ($item->type <= 1) ? $item->amount : 0; // Compute community tax only
            return $carry + $amount;
        }, 0);

        return view('printables.transactions', compact('transactions','total'));
    }
}
