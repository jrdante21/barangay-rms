<?php

namespace App\Http\Controllers\Prints;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Transaction;
use App\Models\Official;
use App\Models\OfficialSignatory;

class DocumentController extends Controller
{
    public function __invoke(Request $request)
    {
        $document = Transaction::with('resident')
        ->whereNotIn('type', [1,11])
        ->where('type', $request->type)
        ->relationsByType($request->type)
        ->findOrFail($request->id);

        // return $document;

        $documentFile = 'doc_'.Str::padLeft($document->type, 2, '0');

        $signatories = OfficialSignatory::with('official')->where('type', $document->type)->get();
        $captain = Official::where('position', 1)->first();

        return view('printables.document', compact(
            'document',
            'documentFile',
            'signatories',
            'captain'
        ));
    }
}
