import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { createStore } from 'vuex'
import axios from 'axios'
import naive from '~/naive.js'
import App from '~/components/App.vue'
import AppLayout from '~/components/admin/AppLayout.vue'
import AppLayout2 from '~/components/resident/AppLayout.vue'
import ProfileDescription from '~/components/utils/ProfileDescription.vue'

// Router
const router = createRouter({
    history: createWebHistory(),
    routes: [
        // Resident Panel
        {
            path: '',
            name: 'resident-login',
            component: () => import(/* webpackChunkName: "js/webpack/resident-login" */  '~/components/resident/Login.vue'),
        },{
            path: '/register',
            name: 'resident-register',
            component: () => import(/* webpackChunkName: "js/webpack/resident-register" */  '~/components/resident/Register.vue'),
        },{
            path: '/email-verify',
            name: 'resident-verify',
            component: () => import(/* webpackChunkName: "js/webpack/resident-verify" */  '~/components/resident/EmailVerify.vue'),
        },{
            path: '/resident/panel',
            component: AppLayout2,
            children: [
                {
                    path: '',
                    name: 'resident-home',
                    component: () => import(/* webpackChunkName: "js/webpack/resident-home" */  '~/components/resident/Home.vue'),
                },{
                    path: 'requests',
                    name: 'resident-requests',
                    component: () => import(/* webpackChunkName: "js/webpack/resident-requests" */  '~/components/resident/Requests.vue'),
                }
            ]
        },
        
        // Admin Panel
        {
            path: '/admin',
            name: 'login',
            component: () => import(/* webpackChunkName: "js/webpack/login" */  '~/components/admin/Login.vue'),
        },{
            path: '/admin/panel',
            component: AppLayout,
            children: [
                // {
                //     path: '',
                //     name: 'home',
                //     component: () => import(/* webpackChunkName: "js/webpack/home" */  '~/components/admin/Home.vue'),
                // },
                {
                    path: '',
                    name: 'home',
                    component: () => import(/* webpackChunkName: "js/webpack/dashboard" */  '~/components/admin/Dashboard.vue'),
                },{
                    path: 'residents',
                    name: 'residents',
                    component: () => import(/* webpackChunkName: "js/webpack/residents" */  '~/components/admin/Residents.vue'),
                },{
                    path: 'resident/:id',
                    name: 'resident',
                    props: true,
                    component: () => import(/* webpackChunkName: "js/webpack/residents" */  '~/components/admin/Resident.vue'),
                },{
                    path: 'transaction-document/:type',
                    name: 'transaction-document',
                    props: true,
                    component: () => import(/* webpackChunkName: "js/webpack/transaction" */  '~/components/admin/Transactions.vue'),
                },{
                    path: 'officials',
                    name: 'officials',
                    component: () => import(/* webpackChunkName: "js/webpack/officials" */  '~/components/admin/Officials.vue'),
                    beforeEnter: () => {
                        if (window.$user.role == 'official') return {name:'home'}
                    }
                },{
                    path: 'administrators',
                    name: 'administrators',
                    component: () => import(/* webpackChunkName: "js/webpack/admins" */  '~/components/admin/Admins.vue'),
                    beforeEnter: () => {
                        if (!window.$user.is_super) return {name:'home'}
                    }
                }
            ]
        }
    ]
})
router.beforeEach((to) => {
    var name = ''
    switch (to.name) {
        case 'transaction-document':
            name = window.$documentTypes[to.params.type]
            break;

        default:
            name = to.name.split('-').map(e => e.charAt(0).toUpperCase() + e.slice(1)).join(' ')
            break;
    }

    document.title = window.$barangay?.barangay+' BMIS | '+name
})

// API Config
const createAxios = (baseURL, isAdmin=false) => {
    const API = axios.create({
        baseURL,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            // 'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
        },
    })

    API.interceptors.response.use(r => { return r }, error => {
        switch (error.response?.status) {
            case 422:
                // Do nothing
                break;

            case 401:
                window.location.replace(isAdmin ? '/admin' : '/')
                break;
        
            case 403:
                window.$messageAlert.error("You can't do this action.", "Unauthorized action")
                break;
    
            case 404:
                window.$messageAlert.error("Sorry we can't find your request at this time.", "Action not found")
                break;

            default:
                window.$messageAlert.error("Try again later.", "Something went wrong")
                break;
        }
    
        return Promise.reject(error)
    })

    // API.defaults.withCredentials = true

    return API
}
window.$api = createAxios('/api-admin', true)
window.$api2 = createAxios('/api-resident')
window.$api3 = createAxios('/api')

// Store
const store = createStore({
    state() {
        return {
            user: window.$user,
            notifications: [],
        }
    },
    mutations: {
        setUser(state, data) {
            state.user = data
        },
        setNotifications(state, data) {
            state.notifications = data
        }
    },
    getters: {
        getNotification: (state) => (type) => {
            return state.notifications.find(el => el.type == type)
        }
    },
    actions: {
        async getNotifications({ commit }) {
            const { data } = await window.$api.get('/notifications')
            commit('setNotifications', data)
        }
    }
})

const app = createApp(App)
app.component('ProfileDescription', ProfileDescription)
app.use(naive)
app.use(store)
app.use(router)
app.mount('#app')