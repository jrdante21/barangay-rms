@extends('layouts.app')
@section('title', "{$barangaySettings['barangay']} BMIS")
@php
    if (auth()->check()) {
        $user = auth()->user();
    } elseif (auth()->guard('official')->check()) {
        $user = auth()->guard('official')->user();
    } else {
        $user = null;
    }
@endphp
@section('content')
    <div id="app"></div>
    <script>
        window.$documentTypes = @json($documentTypes);
        window.$officialPositions = @json($officialPositions);
        window.$user = @json($user);
        window.$barangay = @json($barangaySettings);
    </script>
@endsection
@section('scripts')
    <script src="{{ mix('js/app.js') }}" defer></script>
@endsection