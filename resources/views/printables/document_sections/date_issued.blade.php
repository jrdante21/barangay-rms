<p class="indent">
    Issued this <span class="underline">{!! now()->parse($document->issued_at)->format('j\<\s\u\p\>S\<\/\s\u\p\>') !!}</span> 
    day of <span class="underline">{{ now()->parse($document->issued_at)->format('F Y') }}</span> , at Barangay {{ $barangaySettings->address }}.
</p>