<div class="{{ $flex ?? 'flex-end' }}">
    <div>
        @foreach ($signatories as $signatory)
        <div class="text-center" style="margin-bottom: 10px;">
            <div><b class="underline">{{ $signatory->official->fullname }}</b></div>
            <div><i>{{ $signatory->official->position_str }}</i></div>
        </div>
        @endforeach
    </div>
</div>