<p class="indent">
    <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
    of legal age, {{ strtolower($document->resident->civil_status_str) }} and a resident of Barangay {{ $barangaySettings->address }}.
    This is to certify further that he/she is of good moral character and a law abiding citizen in this community.
</p>