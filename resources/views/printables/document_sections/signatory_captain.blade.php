@if (!empty($captain))
    <div class="{{ $flex ?? 'flex-end' }}">
        <div class="text-center">
            <div><b class="underline">{{ $captain->fullname }}</b></div>
            <div><i>{{ $captain->position_str }}</i></div>
        </div>
    </div>
@endif