@extends('layouts.print')
@section('title', 'Transaction Report')
@section('heading', 'Transaction Report')
@section('content')
    <table class="table">
        <thead>
            <tr>
                <th>Resident Name</th>
                <th>Type</th>
                <th class="collapse">CTC No.</th>
                <th class="collapse">Amount</th>
                <th class="collapse">Date Issued</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($transactions as $transaction)
            <tr>
                <td>{{ $transaction->resident->fullname }}</td>
                <td>{{ $transaction->title }}</td>
                <td class="collapse">{{ $transaction->ctc_number ?? 'N/A' }}</td>
                <td class="collapse" align="right">{{ 
                    ($transaction->type <= 1) ? number_format($transaction->amount, 2) : 'N/A'; 
                }}</td>
                <td class="collapse">{{ now()->parse($transaction->issued_at)->format('F d, Y') }}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="4" align="right"><b>Total</b></td>
                <td align="right"><b>{{ number_format($total, 2) }}</b></td>
                <td></td>
            </tr>
        </tbody>
    </table>
@endsection