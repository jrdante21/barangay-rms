@extends('layouts.print')
@section('title', $document->title)
@section('heading', 'Office of the Punong Barangay')
@section('content')

    @include('printables.document_bodies.'.$documentFile)
    
    @if ($document->type != 9)
        {{-- Official Signatories --}}
        <div style="margin-top:30px;">
            @if (count($signatories) >= 1)
                @include('printables.document_sections.signatories')
            @else
                @include('printables.document_sections.signatory_captain')
            @endif
        </div>

        {{-- Numbers and Date --}}
        @include('printables.document_sections.document_numbers')
    @endif

@endsection