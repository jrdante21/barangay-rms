{{-- Business Permit --}}

@include('printables.document_sections.title')

@include('printables.document_sections.receipient')

<p class="indent">
    <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
    of legal age, single and owner of <b class="underline">{{ $document->resident_itemable->name }}</b> at 
    {{ $document->resident_itemable->address_str }} is given permit to operate his/her establishment/business subject
    however to the provision of our existing laws and ordinance, and rules & regulation governing the operation and maintenance of the same.
</p>
<p class="indent">
    This permit may be revoked at any time when necessary to protect the public interest or violation
    of any provision
</p>

@include('printables.document_sections.date_issued')