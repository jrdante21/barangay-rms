{{-- Barangay Clearance --}}

@include('printables.document_sections.title')

@include('printables.document_sections.receipient')

@include('printables.document_sections.paragraph_first')

<p class="indent">Records of this office show that as to this date he/she has not been accuse of any crime or any pending obligation.</p>
<p class="indent">This clearance is issued upon the request of the above-mentioned name for whatever legal purpose it may deem serve.</p>

@include('printables.document_sections.date_issued')
