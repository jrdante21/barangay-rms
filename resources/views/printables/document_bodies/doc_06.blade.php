{{-- Business Clearance --}}

@include('printables.document_sections.title')

@include('printables.document_sections.receipient')

<p class="indent">
    <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
    resident of Barangay {{ $barangaySettings->address }}. Has no pending obligation in our Purok.
</p>
<p>
    <div class="flex-center text-center" style="gap: 30px">
        <div>Owner of:</div>
        <div>
            <div><b class="underline">{{ $document->resident_itemable->name }}</b></div>
            <div><i>Name type of Business</i></div>
        </div>
    </div>
</p>
<p>
    <div class="flex-center text-center">
        <div>
            <div><b class="underline">{{ $document->resident_itemable->address_str }}</b></div>
            <div><i>Place of Business</i></div>
        </div>
    </div>
</p>
<p class="indent">
    Provided however, that all rules, regulations, and ordinances of this Barangay are being observed
    and complied with accordingly
</p>

@include('printables.document_sections.date_issued')