{{-- COVID Travel Pass --}}

<p class="text-lg">NAME: {{ $document->resident->fullname }}</p>
<p class="text-lg">ADDRESS: {{ $document->resident->address }}</p>
<p class="text-lg">DESTINATION: {{ $document->destination_str }}</p>
<p class="text-lg">PURPOSE: {{ $document->purpose }}</p>
<p class="text-lg">DATE: {{ now()->parse($document->issued_at)->format('F d, Y') }}</p>

<div style="margin:10px 0px;">
    @include('printables.document_sections.signatory_captain', ['flex' => 'flex-center'])
</div>

<div class="text-center" style="color:red">
    <div class="uppercase font-bold" style="text-decoration: underline;">This local travel pass is valid only for 1 day only</div>
    <div class="italic">
        Note: <span class="uppercase">Erasures, alteration, no dry seal</span> and <span class="uppercase">signature</span> <br>
        Make this <span class="uppercase">Quarantine pass invalid.</span>
    </div>
</div>
            