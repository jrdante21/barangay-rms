{{-- Barangay Certificate --}}

@include('printables.document_sections.title')

@include('printables.document_sections.receipient')

@include('printables.document_sections.paragraph_first')

<p class="indent">This certificate is issued upon the request of the above-mentioned name for whatever legal purpose it may deem serve.</p>

@include('printables.document_sections.date_issued')
