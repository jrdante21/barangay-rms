{{-- Summons / Blotter --}}

<table style="width: 100%;">
    <tr>
        <td class="collapse text-center">
            <div>
                <div><b class="underline">{{ $document->resident->fullname }}</b></div>
                <div><i>COMPLAINANT</i></div>
            </div>
            <div style="margin: 10px 0px;">
                <b>Against</b>
            </div>
            <div>
                <div><b class="underline">{{ $document->summon->respondent }}</b></div>
                <div><i>RESPONDENT</i></div>
            </div>
        </td>
        <td></td>
        <td class="collapse" style="vertical-align: top;">
            <div>Barangay Case No: <b class="underline">{{ $document->ctc_number }}</b></div>
            <div>For: <b class="underline">{{ $document->purpose }}</b></div>
        </td>
    </tr>
</table>

@include('printables.document_sections.title')

<p><b class="uppercase">To {{ $document->summon->respondent }}:</b></p>

<p class="indent">
    You are hereby summoned to appear before me in person, together with your witness, on the
    <b class="underline">{!! now()->parse($document->scheduled_at)->format('j\<\s\u\p\>S\<\/\s\u\p\>') !!}</b> 
    day of <b class="underline">{{ now()->parse($document->scheduled_at)->format('F Y') }}</b>,
    at <b class="underline">{{ now()->parse($document->scheduled_at)->format('h:i A') }}</b>
    then and there to answer to a complaint made before me, copy of which is attached hereto,
    for mediation / conciliation of your dispute with complainant.
</p>

<p class="indent">
    You are hereby warned that if you refuse or willfully fail to appear in obedience to this simmons,
    you maybe bared from filling any counter arising to said complaints. Fail not or else face punishment
    as for contempt of court.
</p>

{{-- Date Issued --}}
@include('printables.document_sections.date_issued')

{{-- Dashed Border --}}
<div style="border-top: 2px dashed black; margin: 20px 0px;"></div>

{{-- Agreement --}}
<table cellspacing="5px">
    <tr>
        <td class="collapse">__________1.</td>
        <td>Handling to him said summon in person.</td>
    </tr>
    <tr>
        <td>__________2.</td>
        <td>Handling to him said summon but he refuse to receive.</td>
    </tr>
    <tr>
        <td>__________3.</td>
        <td>
            Leaving said summon to his dwelling with ______________________________<br>
            A Person of suitable age and discretion residing there in.
        </td>
    </tr>
</table>

{{-- Witnesses Signatures --}}
<table cellspacing="5px" style="width: 100%;">
    @for ($i = 1; $i <= 2; $i++)
    <tr style="height: 30px">
        <td style="width: 30%; border-bottom: solid 1px black;"></td>
        <td style="width: 40%"></td>
        <td style="width: 30%; border-bottom: solid 1px black;"></td>
    </tr>
    @endfor
</table>