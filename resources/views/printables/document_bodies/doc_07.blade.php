{{-- Purok Clearance --}}

@include('printables.document_sections.title')

@include('printables.document_sections.receipient')

<p class="indent">
    <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>, 
    resident of Barangay {{ $barangaySettings->address }}. Has no pending obligation in our Purok.
</p>
<p class="indent">He/she can now secure Barangay Clearance.</p>

@include('printables.document_sections.date_issued')