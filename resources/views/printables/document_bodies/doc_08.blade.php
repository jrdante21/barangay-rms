{{-- Certification --}}

@include('printables.document_sections.title')

@include('printables.document_sections.receipient')

<p class="indent">
    <b>THIS IS TO CERTIFY that I officially know <span class="underline">{{ $document->resident->fullname }}</span></b>,
    of legal age, {{ strtolower($document->resident->civil_status_str) }} and a resident of Barangay {{ $barangaySettings->address }}.
    This is to certify further the he is a legal owner of a <span class="underline">{{ $document->resident_itemable->details[0]['value'] }}</span>
    with the following description:
</p>
<p>
    @foreach ($document->resident_itemable->details as $detail)
        @if (!$loop->first)
            <div class="indent uppercase">{{ $detail['label'] }}: <b>{{ $detail['value'] }}</b></div>
        @endif
    @endforeach
</p>
<p class="indent">{{ $document->purpose }}</p>
<p class="indent">This certification is issued upon the request of the above mentioned name for whatever legal purpose it rnay deemed serve.</p>

@include('printables.document_sections.date_issued')