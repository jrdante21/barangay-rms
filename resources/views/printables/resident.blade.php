@extends('layouts.print')
@section('title', $resident->fullname)
@section('heading', 'Resident Information')
@section('content')
    <p class="mb-5px"><b>General Information</b></p>
    <div class="flex">
        @if (!empty($resident->photo))
            <img style="width: 7rem;" src="/images/profiles/{{ $resident->photo }}" alt="Resident Photo">
        @else
            <div class="flex" style="width: 7rem; height: 7rem; border: 1px solid #ccc">
                <div style="flex: 1" class="text-center">No Photo</div>
            </div>
        @endif
        <div style="flex:1">
            <table cellspacing="5px" style="width: 100%">
                <tr>
                    <td class="collapse">Name:</td>
                    <td class="border-bottom"><b>{{ $resident->fullname }}</b></td>
                    <td class="collapse">Gender:</td>
                    <td class="border-bottom"><b>{{ $resident->gender_str }}</b></td>
                    <td class="collapse">Civil Status:</td>
                    <td class="border-bottom"><b>{{ $resident->civil_status_str }}</b></td>
                </tr>
            </table>
            <table cellspacing="5px" style="width: 100%">
                <tr>
                    <td class="collapse">Email:</td>
                    <td class="border-bottom"><b>{{ $resident->email }}</b></td>
                    <td class="collapse">Birthdate:</td>
                    <td class="border-bottom"><b>{{ now()->parse($resident->bday)->format('F d, Y') }}</b></td>
                    <td class="collapse">Age:</td>
                    <td class="border-bottom"><b>{{ $resident->age }}</b></td>
                    <td class="collapse">Years Resided:</td>
                    <td class="border-bottom"><b>{{ $resident->years_resided }}</b></td>
                </tr>
            </table>
            <table cellspacing="5px" style="width: 100%">
                <tr>
                    <td class="collapse">Address:</td>
                    <td class="border-bottom"><b>{{ $resident->address }}</b></td>
                </tr>
            </table>
        </div>
    </div>

    {{-- Educational Background --}}
    @if (count($resident->educations) >= 1)
        <p class="mb-5px"><b>Educational Background</b></p>
        <table class="table">
            <thead>
                <tr>
                    <th>Level</th>
                    <th>Program / Course</th>
                    <th>School Name</th>
                    <th>School Address</th>
                    <th>Year Graduated</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resident->educations as $education)
                    <tr>
                        <td>{{ $education->level_str }}</td>
                        <td>{{ $education->course }}</td>
                        <td>{{ $education->school_name }}</td>
                        <td>{{ $education->address }}</td>
                        <td>{{ now()->parse($education->graduated_at)->format('F Y') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    {{-- Femily Background --}}
    <p class="mb-5px"><b>Family Background</b></p>
    <table cellspacing="5px" style="width: 100%">
        <tr>
            <td class="collapse">Father's Name:</td>
            <td class="border-bottom"><b>{{ $resident->father }}</b></td>
        </tr>
        <tr>
            <td class="collapse">Mother's Name:</td>
            <td class="border-bottom"><b>{{ $resident->mother }}</b></td>
        </tr>
        <tr>
            <td class="collapse">Spouse's Name:</td>
            <td class="border-bottom"><b>{{ $resident->spouse }}</b></td>
        </tr>
    </table>
    @if (count($resident->children) >= 1)
        <table style="margin-top: 10px;" class="table">
            <thead>
                <tr>
                    <th>Children Name</th>
                    <th>Gender</th>
                    <th>Civil Status</th>
                    <th>Birthdate</th>
                    <th>Age</th>
                    <th>School</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resident->children as $child)
                    <tr>
                        <td>{{ $child->fullname }}</td>
                        <td>{{ $child->gender_str }}</td>
                        <td>{{ $child->civil_status_str }}</td>
                        <td>{{ now()->parse($child->bday)->format('F d, Y') }}</td>
                        <td>{{ $child->age }}</td>
                        <td style="font-size: 12px;">
                            @if ($child->is_student)
                                <div>Level: <b>{{ $child->level_str }}</b></div>
                                <div>School: <b>{{ $child->school_name }}</b></div>
                                <div>Address: <b>{{ $child->school_address_str }}</b></div>
                            @else
                                N/A
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    {{-- Siblings --}}
    @if (count($resident->siblings) >= 1)
        <p class="mb-5px"><b>Siblings</b></p>
        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resident->siblings as $sibling)
                    <tr>
                        <td>{{ $sibling->fullname }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    {{-- Businesses --}}
    @if (count($resident->businesses) >= 1)
        <p class="mb-5px"><b>Businesses</b></p>
        <table class="table">
            <thead>
                <tr>
                    <th>Business Name</th>
                    <th>Business Address</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resident->businesses as $business)
                    <tr>
                        <td>{{ $business->name }}</td>
                        <td>{{ $business->address_str }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    {{-- Jobs --}}
    @if (count($resident->jobs) >= 1)
        <p class="mb-5px"><b>Work / Job History</b></p>
        <table class="table">
            <thead>
                <tr>
                    <th>Job Title / Position</th>
                    <th>Company Name</th>
                    <th>Date Started - Date Ended</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resident->jobs as $job)
                    <tr>
                        <td>{{ $job->position }}</td>
                        <td>{{ $job->company_name }}</td>
                        <td>{{ $job->date_range }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    {{-- Assets --}}
    @if (count($resident->assets) >= 1)
        <p class="mb-5px"><b>Assets</b></p>
        <table class="table">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Descriptions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resident->assets as $asset)
                    <tr>
                        <td>{{ $asset->type_str }}</td>
                        <td class="uppercase">
                            @foreach ($asset->details as $detail)
                                <span style="margin-right: 5px;">{{ $detail['label'] }}: <b>{{ $detail['value'] }}</b></span>
                            @endforeach
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection