@component('mail::message', ['address' => $address])
    <table cellspacing="0" cellpadding="0" width="100%" style="font-family:Arial, Helvetica, sans-serif;">
        <tr>
            <td align="center">
                <h1 style="font-weight:bold; font-size:24px; text-align:center;">
                    Email Verification Code
                </h1>
            </td>
        </tr>
        <tr>
            <td align="center">
                <p style="font-size:16px; text-align:center;">
                    Please copy and use this code to verify your account and login.
                    Verify your code <a href="{{ config('app.url') }}/email-verify" target="_blank">here.</a>
                </p>
            </td>
        </tr>
    </table>
    @component('mail::panel')
        <p style="font-weight:bold; font-size:30px; text-align:center;">
            {{ $code }}
        </p>
    @endcomponent
@endcomponent