@extends('layouts.app')
@section('title', "{$barangaySettings['barangay']} BMIS")
@section('content')
<div id="app"></div>
<script>
    window.$documentTypes = @json($documentTypes);
    window.$officialPositions = @json($officialPositions);
    window.$user = @json($user ?? null);
    window.$barangay = @json($barangaySettings);
</script>
@endsection
@section('scripts')
    <script src="{{ mix('js/app.js') }}" defer></script>
@endsection