<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\ResidentController;
use App\Http\Controllers\API\ResidentEducationController;
use App\Http\Controllers\API\ResidentChildrenController;
use App\Http\Controllers\API\ResidentBusinessController;
use App\Http\Controllers\API\ResidentJobController;
use App\Http\Controllers\API\ResidentAssetController;
use App\Http\Controllers\API\ResidentSiblingController;
use App\Http\Controllers\API\TransactionController;
use App\Http\Controllers\API\OfficialController;
use App\Http\Controllers\API\OfficialSignatoryController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\NotificationController;

use App\Http\Controllers\Prints\DocumentController as PrintDocument;
use App\Http\Controllers\Prints\TransactionController as PrintTransaction;
use App\Http\Controllers\Prints\ResidentController as PrintResident;

use App\Http\Controllers\Logins\AdminController as LoginAdmin;
use App\Http\Controllers\Logins\ResidentController as LoginResident;

use App\Http\Controllers\APIResident\ResidentController as APIResidentUser;
use App\Http\Controllers\APIResident\RequestController as APIResidentRequest;

use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test-domain', TestController::class);

// Resident

Route::get('/', [LoginResident::class, 'index'])->name('resident.login');
Route::view('/register', 'resident');
Route::view('/email-verify', 'resident');

Route::prefix('resident')->name('resident.')->group(function()
{
    Route::post('/login', [LoginResident::class, 'login']);
    Route::get('/logout', [LoginResident::class, 'logout']);
    Route::get('/panel/{any?}', [APIResidentUser::class, 'index'])->where('any','.*')->middleware('auth:resident')->name('panel');
});

Route::prefix('api-resident')->middleware(['auth:resident','verified'])->group(function()
{
    Route::post('/update-password', [APIResidentUser::class, 'updatePassword']);
    Route::apiResource('requests', APIResidentRequest::class);
});

// Admin

Route::prefix('admin')->name('admin.')->group(function()
{
    Route::get('/', [LoginAdmin::class, 'index'])->name('login');
    Route::post('/login', [LoginAdmin::class, 'login']);
    Route::get('/logout', [LoginAdmin::class, 'logout']);

    Route::middleware('auth:web,official')->group(function()
    {
        Route::view('/panel/{any?}', 'admin')->where('any','.*')->name('panel');

        Route::prefix('print')->group(function()
        {
            Route::get('/documents', PrintDocument::class);
            Route::get('/transactions', PrintTransaction::class);
            Route::resource('residents', PrintResident::class)->only(['index','show']);
        });
    });
});

Route::prefix('api-admin')->middleware('auth:web,official')->group(function()
{
    Route::get('/dashboard', DashboardController::class);
    Route::get('/notifications', NotificationController::class);

    Route::post('/resident-change-password/{resident}', [ResidentController::class, 'updatePassword']);
    Route::post('/resident-family/{resident}', [ResidentController::class, 'updateFamily']);
    Route::post('/resident-photo/{resident}', [ResidentController::class, 'updatePhoto']);
    Route::post('/resident-verification/{resident}', [ResidentController::class, 'verification']);
    Route::post('/admin-activate/{admin}', [AdminController::class, 'restore']);
    Route::post('/official-signatories', OfficialSignatoryController::class);
    Route::post('/official-activate/{official}', [OfficialController::class, 'restore']);
    Route::post('/transaction-update-payment/{transaction}', [TransactionController::class, 'updatePayment']);

    Route::apiResource('admins', AdminController::class);
    Route::apiResource('officials', OfficialController::class);
    Route::apiResource('residents', ResidentController::class);
    Route::apiResource('residents.businesses', ResidentBusinessController::class)->except(['show'])->shallow();
    Route::apiResource('residents.assets', ResidentAssetController::class)->except(['show'])->shallow();
    Route::apiResource('residents.educations', ResidentEducationController::class)->except(['show','index'])->shallow();
    Route::apiResource('residents.children', ResidentChildrenController::class)->except(['show','index'])->shallow();
    Route::apiResource('residents.jobs', ResidentJobController::class)->except(['show','index'])->shallow();
    Route::apiResource('residents.siblings', ResidentSiblingController::class)->except(['show','index'])->shallow();
    Route::apiResource('transactions', TransactionController::class);

    Route::prefix('users')->group(function()
    {
        Route::post('/update-username', [UserController::class, 'updateUsername']);
        Route::post('/update-password', [UserController::class, 'updatePassword']);
    });
});
