<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admins')->delete();
        
        \DB::table('admins')->insert(array (
            0 => 
            array (
                'created_at' => '2021-08-01 06:00:53',
                'deleted_at' => NULL,
                'id' => 1,
                'is_super' => 1,
                'password' => '$2y$10$XxAeYU1g/QZssK0q9lBdJu70JwhhbmTJYz55MNwP8b0f3YFiLewa.',
                'updated_at' => '2022-02-26 09:59:47',
                'username' => 'superadmin',
            ),
            1 => 
            array (
                'created_at' => '2021-09-03 20:50:08',
                'deleted_at' => NULL,
                'id' => 2,
                'is_super' => 0,
                'password' => '$2y$10$OCbLHbNVtxkZ4tbEx0NBfOoXdXevf40JIuoAPQZ9GTPe4WyJCGMN.',
                'updated_at' => '2022-02-25 13:46:23',
                'username' => 'admin123',
            ),
            2 => 
            array (
                'created_at' => '2022-02-25 12:21:35',
                'deleted_at' => NULL,
                'id' => 3,
                'is_super' => 0,
                'password' => '$2y$10$XxAeYU1g/QZssK0q9lBdJu70JwhhbmTJYz55MNwP8b0f3YFiLewa.',
                'updated_at' => '2022-02-25 13:46:14',
                'username' => 'admin156',
            ),
        ));
        
        
    }
}