<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resident_id');
            $table->tinyInteger('type');
            $table->string('resident_itemable_type')->nullable();
            $table->unsignedBigInteger('resident_itemable_id')->nullable();
            $table->string('ctc_number', 20)->nullable();
            $table->string('or_number', 20)->nullable();
            $table->double('amount', 8, 2);
            $table->text('purpose')->nullable();
            $table->date('issued_at')->nullable();
            $table->date('paid_at')->nullable();
            $table->date('returned_at')->nullable();
            $table->dateTime('requested_at')->nullable();
            $table->boolean('is_requested');
            $table->timestamps();

            $table->index(['resident_itemable_type', 'resident_itemable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
