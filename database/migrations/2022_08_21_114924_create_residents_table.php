<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fname', 50);
            $table->string('mname', 50);
            $table->string('lname', 50);
            $table->string('email')->nullable()->unique();
            $table->string('password')->nullable();
            $table->string('photo', 100)->nullable();
            $table->tinyInteger('gender');
            $table->tinyInteger('civil_status');
            $table->date('bday');
            $table->string('home_number', 10)->nullable();
            $table->tinyInteger('purok');
            $table->string('barangay', 100);
            $table->string('municipality', 100);
            $table->string('province', 100);
            $table->double('years_resided');
            $table->boolean('is_voter')->default(false);
            $table->boolean('is_household')->default(false);
            $table->json('father_name')->nullable();
            $table->json('mother_name')->nullable();
            $table->json('spouse_name')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residents');
    }
}
