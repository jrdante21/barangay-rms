<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidentChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resident_children', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resident_id');
            $table->string('fname', 200);
            $table->string('mname', 200);
            $table->string('lname', 200);
            $table->tinyInteger('gender');
            $table->tinyInteger('civil_status');
            $table->date('bday');
            $table->tinyInteger('school_level')->nullable();
            $table->string('school_name', 200)->nullable();
            $table->json('school_address')->nullable();
            $table->tinyInteger('is_student');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resident_children');
    }
}
